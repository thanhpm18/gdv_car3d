#include <GL/freeglut.h>
#include <GL/glut.h>   // laedt alles f�r OpenGL
#include <GL/soil.h>
#include "texture.h"

GLuint tex_2d;         // Textur-ID

void textureInit(const char* filename) {
	// Textur einlesen usw.
	tex_2d = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
	glBindTexture(GL_TEXTURE_2D, tex_2d);
	// Transparenz einsetzen (z.B. fuer Billboards)
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}