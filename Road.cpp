#include <iostream>
#include <GL/freeglut.h>         //l�dt alles f�r OpenGL
#include <GL/glut.h>   // laedt alles f�r OpenGL
#include "Road.h"
#include "Tree.h"

using namespace std;

void Road(GLfloat position1, GLfloat position2, GLfloat y_winkel)
{
    glNormal3f(0.0f, 1.0f, 0.0f); // light Normal vec
    GLfloat x, z;
    glPushMatrix();
        glPushMatrix();
            glTranslatef(position1, 0.0f, 0.0f);
            glBegin(GL_QUADS);
                glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
                x = -27.0f;
                while ((x + 0.5f) < -9)
                {
                    z = .3f + (.075f / 2);
                    glVertex3f(x, -.22f, z);	//bottom left
                    glVertex3f(x + .5f, -.22f, z);	//bottom right
                    glVertex3f(x + .5f, -.22f, z - .075f);	//top right
                    glVertex3f(x, -.22f, z - .075f);	//top left
                    z -= .6f;
                    glVertex3f(x, -.22f, z);	//bottom left
                    glVertex3f(x + .5f, -.22f, z);	//bottom right
                    glVertex3f(x + .5f, -.22, z - .075f);	//top right
                    glVertex3f(x, -.22f, z - .075f);	//top left
                    x += 1.0f;
                }
            glEnd();

            glPushMatrix();
                glTranslatef(-27.0f, .3f, 1.5f);
                Tree(y_winkel);
            glPopMatrix();

            glPushMatrix();
                glTranslatef(-18.0f, .3f, 1.5f);
                Tree(y_winkel);
            glPopMatrix();
        glPopMatrix();

        glPushMatrix();
            glTranslatef(position2, 0.0f, 0.0f);
        
            glBegin(GL_QUADS);
                glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
                x = -9.;
                while ((x + 0.5f) < 9)
                {
                    z = .3f + (.075f / 2);
                    glVertex3f(x, -.22f, z);	//bottom left
                    glVertex3f(x + .5f, -.22f, z);	//bottom right
                    glVertex3f(x + .5f, -.22f, z - .075f);	//top right
                    glVertex3f(x, -.22f, z - .075f);	//top left
                    z -= .6f;
                    glVertex3f(x, -.22f, z);	//bottom left
                    glVertex3f(x + .5f, -.22f, z);	//bottom right
                    glVertex3f(x + .5f, -.22f, z - .075f);	//top right
                    glVertex3f(x, -.22f, z - .075f);	//top left
                    x += 1.0f;
                }
            glEnd();
                glPushMatrix();
                    glTranslatef(0.0f, .3f, -1.5f);
                    Tree(y_winkel);
                glPopMatrix();
                
                glPushMatrix();
                    glTranslatef(-9.0f, .3f, -1.5f);
                    Tree(y_winkel);
                glPopMatrix();
        glPopMatrix();
        
        glPushMatrix();
            glBegin(GL_QUADS);
                glNormal3f(0.0f, 1.0f, 0.0f); // light Normal vec
                glColor4f(0.4f, 0.4f, 0.4f, 1.0f); //GRAU
                glVertex3f(-10.0f, -.225f, .9f);	//bottom left
                glVertex3f(10.0f, -.225f, .9f);	//bottom right
                glVertex3f(10.0f, -.225f, -.9f);	//top right
                glVertex3f(-10.0f, -.225f, -.9f);	//top left
            glEnd();
        glPopMatrix();
    glPopMatrix();
}
