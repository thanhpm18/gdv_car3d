#include <iostream>
#include <GL/freeglut.h>
#include "Car.h"
#include "Cylinder.h"

using namespace std;

enum COLOUR { BLACK, WHITE, RED, GREEN, BLUE, CYAN, YELLOW, MINT, GLASS, MIX=99 };

void setColour(int colour) {
	switch (colour)
	{
	case COLOUR::BLACK:
		glColor4f(0.0f, 0.0f, 0.0f, 1.0f); //SCHWARZ
		break;
	case COLOUR::WHITE:
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
		break;
	case COLOUR::RED:
		glColor4f(1.0f, 0.0f, 0.0f, 1.0f);	//ROT
		break;
	case COLOUR::GREEN:
		glColor4f(0.0f, 1.0f, 0.0f, 1.0f); //GRUEN
		break;
	case COLOUR::BLUE:
		glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
		break;
	case COLOUR::CYAN:
		glColor4f(0.0f, 1.0f, 1.0f, 1.0f);	//CYAN
		break;
	case COLOUR::YELLOW:
		glColor4f(1.0f, 1.0f, 0.0f, 1.0f); //GELB
		break;
	case COLOUR::MINT:
		glColor4f(0.741f, 1.f, 0.741f, 1.0f); //MINT
		break;
	case COLOUR::GLASS:
		glColor4f(0.8f, 0.8f, 0.8f, 0.9f); //GLASS
		break;
	default:
		break;
	}
}

void Car(int colour, GLfloat angle) {
	glPushMatrix();

	/* Kontur */
	glLineWidth(3.f);
	setColour(colour);
	if (colour == COLOUR::WHITE) {
		setColour(COLOUR::BLACK);
	}
	glBegin(GL_LINE_LOOP);		// Decke
	glNormal3f(0.0f, 1.0f, 0.0f); // light Normal vec
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(-.2, .15, .2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(.2, .15, .2);	//bottom right
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(.2, .15, -.2);	//top right
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(-.2, .15, -.2);	//top left
	glEnd();
	glBegin(GL_LINE_LOOP);		// Boden
	glNormal3f(0.0f, -1.0f, 0.0f);	// light Normal vec
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(-.7, -.15, .2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(.5, -.15, .2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(.5, -.15, -.2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(-.7, -.15, -.2);
	glEnd();
	glBegin(GL_LINE_LOOP);		// vorne Fenster
	glNormal3f(-0.144f, 0.192f, 0.0f);	// light Normal vec
	glVertex3f(-.4, 0., .2);	//bottom left
	glVertex3f(-.2, .15, .2);
	glVertex3f(-.2, .15, -.2);
	glVertex3f(-.4, 0., -.2);
	glEnd();
	glBegin(GL_LINE_LOOP);	//hinter Fenster
	glNormal3f(0.207f, 0.137f, 0.0f);	// light Normal vec
	glVertex3f(.2, .15, .2);	//bottom left
	glVertex3f(.3, 0., .2);
	glVertex3f(.3, 0., -.2);
	glVertex3f(.2, .15, -.2);
	glEnd();
	glBegin(GL_LINE_LOOP);	//Autohaube
	glNormal3f(0.0f, 1.0f, 0.0f); // light Normal vec
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(-.7, 0., .2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(-.4, 0., .2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(-.4, 0., -.2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(-.7, 0., -.2);
	glEnd();
	glBegin(GL_LINE_LOOP);	//Kofferraumhaube
	glNormal3f(0.0f, 1.0f, 0.0f); // light Normal vec
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(.3, 0., .2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(.5, 0., .2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(.5, 0., -.2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(.3, 0., -.2);
	glEnd();
	glBegin(GL_LINE_LOOP);	//vorne Licht
	glNormal3f(-1.0f, 0.0f, 0.0f);	// light Normal vec
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(-.7, -.15, .2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(-.7, 0., .2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(-.7, 0., -.2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(-.7, -.15, -.2);
	glEnd();
	glBegin(GL_LINE_LOOP);	//hinter Licht
	glNormal3f(1.0f, 0.0f, 0.0f);	// light Normal vec
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(.5, 0., .2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(.5, -.15, .2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(.5, -.15, -.2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(.5, 0., -.2);
	glEnd();
	glBegin(GL_LINE_LOOP);		// linke T�r
	glNormal3f(0.0f, 0.0f, 1.0f);	// light Normal vec
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(-.7, -.15, .2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(.5, -.15, .2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(.5, 0., .2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(-.7, 0., .2);
	glEnd();
	glBegin(GL_LINE_LOOP);
	glVertex3f(-.4, 0., .2);	//bottom left
	glVertex3f(.3, 0., .2);
	glVertex3f(.2, .15, .2);
	glVertex3f(-.2, .15, .2);
	glEnd();
	glBegin(GL_LINES);
	glVertex3f(0., 0., .2);
	glVertex3f(0., .15, .2);
	glEnd();
	glBegin(GL_LINE_LOOP);	//rechte T�r
	glNormal3f(0.0f, 0.0f, -1.0f);	// light Normal vec
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(-.7, -.15, -.2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(.5, -.15, -.2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(.5, 0., -.2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(-.7, 0., -.2);
	glEnd();
	glBegin(GL_LINE_LOOP);
	glVertex3f(-.4, 0., -.2);
	glVertex3f(.3, 0., -.2);
	glVertex3f(.2, .15, -.2);
	glVertex3f(-.2, .15, -.2);
	glEnd();
	glBegin(GL_LINES);
	glVertex3f(0., 0., -.2);
	glVertex3f(0., .15, -.2);
	glEnd();

	/* Fl�che */
	glBegin(GL_QUADS);	// Decke
	glNormal3f(0.0f, 1.0f, 0.0f); // light Normal vec
	setColour(colour);
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(-.2, .15, .2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(.2, .15, .2);	//bottom right
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(.2, .15, -.2);	//top right
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(-.2, .15, -.2);	//top left
	glEnd();

	glBegin(GL_QUADS);	// Boden
	glNormal3f(0.0f, -1.0f, 0.0f);	// light Normal vec
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(-.7, -.15, .2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(.5, -.15, .2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(.5, -.15, -.2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(-.7, -.15, -.2);
	glEnd();

	glBegin(GL_QUADS);	//vorne Fenster
	glNormal3f(-0.144f, 0.192f, 0.0f);	// light Normal vec
	setColour(COLOUR::GLASS);
	glVertex3f(-.4, 0., .2);	//bottom left
	glVertex3f(-.2, .15, .2);
	glVertex3f(-.2, .15, -.2);
	glVertex3f(-.4, 0., -.2);
	glEnd();

	glBegin(GL_QUADS);	//hinter Fenster
	glNormal3f(0.207f, 0.137f, 0.0f);	// light Normal vec
	glVertex3f(.2, .15, .2);	//bottom left
	glVertex3f(.3, 0., .2);
	glVertex3f(.3, 0., -.2);
	glVertex3f(.2, .15, -.2);
	glEnd();

	glBegin(GL_QUADS);	//Autohaube
	setColour(colour);
	glNormal3f(0.0f, 1.0f, 0.0f); // light Normal vec
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(-.7, 0., .2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(-.4, 0., .2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(-.4, 0., -.2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(-.7, 0., -.2);
	glEnd();

	glBegin(GL_QUADS);	//Kofferraumhaube
	glNormal3f(0.0f, 1.0f, 0.0f); // light Normal vec
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(.3, 0., .2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(.5, 0., .2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(.5, 0., -.2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(.3, 0., -.2);
	glEnd();

	glBegin(GL_QUADS);	//vorne Licht
	glNormal3f(-1.0f, 0.0f, 0.0f);	// light Normal vec
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(-.7, -.15, .2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(-.7, 0., .2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(-.7, 0., -.2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(-.7, -.15, -.2);
	glEnd();

	glBegin(GL_QUADS);	//hinter Licht
	glNormal3f(1.0f, 0.0f, 0.0f);	// light Normal vec
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(.5, 0., .2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(.5, -.15, .2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(.5, -.15, -.2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(.5, 0., -.2);
	glEnd();

	glBegin(GL_QUADS);	//linke T�r
	glNormal3f(0.0f, 0.0f, 1.0f);	// light Normal vec
	setColour(colour);
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(-.7, -.15, .2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(.5, -.15, .2);
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
	glVertex3f(.5, 0., .2);
	if (colour == COLOUR::MIX)	glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
	glVertex3f(-.7, 0., .2);

	setColour(COLOUR::GLASS);
	glVertex3f(-.4, 0., .2);
	glVertex3f(.3, 0., .2);
	glVertex3f(.2, .15, .2);
	glVertex3f(-.2, .15, .2);
	glEnd();

	glBegin(GL_QUADS);	//rechte T�r
	glNormal3f(0.0f, 0.0f, -1.0f);	// light Normal vec
	setColour(colour);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(-.7, -.15, -.2);	//bottom left
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(.5, -.15, -.2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(.5, 0., -.2);
	if (colour == COLOUR::MIX)	glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
	glVertex3f(-.7, 0., -.2);

	setColour(COLOUR::GLASS);
	glVertex3f(-.4, 0., -.2);
	glVertex3f(.3, 0., -.2);
	glVertex3f(.2, .15, -.2);
	glVertex3f(-.2, .15, -.2);
	glEnd();

	glPopMatrix();

	glPushMatrix();		//vorne link Rad
	glTranslatef(-.55, -.15, .18);
	glRotatef(angle, 0., 0., 1.);
	CylinderS(.075, .075);
	glPopMatrix();

	glPushMatrix();		//vorne recht Rad
	glTranslatef(-.55, -.15, -.18);
	glRotatef(angle, 0., 0., 1.);
	CylinderS(.075, .075);
	glPopMatrix();

	glPushMatrix();		//hinter link Rad
	glTranslatef(.3, -.15, .18);
	glRotatef(angle, 0., 0., 1.);
	CylinderS(.075, .075);
	glPopMatrix();

	glPushMatrix();		//hinter recht Rad
	glTranslatef(.3, -.15, -.18);
	glRotatef(angle, 0., 0., 1.);
	CylinderS(.075, .075);
	glPopMatrix();
}