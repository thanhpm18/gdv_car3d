#include <iostream>
#include <GL/freeglut.h>         //l�dt alles f�r OpenGL
#include <GL/glut.h>   // laedt alles f�r OpenGL
#include "Tree.h"
#include "texture.h"

void Tree(GLfloat y_winkel)
{
    glPushMatrix();
        glRotatef(y_winkel, 0., 1., 0.);
        glRotatef(90., 0., 1., 0.);
        textureInit("TREE2.PNG");
        glEnable(GL_TEXTURE_2D);
            glBegin(GL_QUADS);
                glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
                glTexCoord2f(0.0f, 0.0f);   glVertex3f(-.6, -.6, 0.f); // bottom left
                glTexCoord2f(1.0f, 0.0f);   glVertex3f(.6, -.6, 0.f);
                glTexCoord2f(1.0f, 1.0f);   glVertex3f(.6, .6, 0.f);
                glTexCoord2f(0.0f, 1.0f);   glVertex3f(-.6, .6, 0.f);
            glEnd();
        glDisable(GL_TEXTURE_2D);
    glPopMatrix();
}
