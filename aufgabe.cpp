// GDV Zusatleistung:   aufgabe.cpp  (Start-Programm)
// Minh Thanh Pham  Matrikelnummer: 762438
// Hai Ninh Bui     Matrikelnummer: 757973
// Last Update: 14.03.2021

#include <iostream>
#include <time.h>
#include <string.h>
#include <string>
#include <GL/freeglut.h>         //lädt alles für OpenGL
#include <GL/glut.h>   // laedt alles für OpenGL
#include <GL/soil.h>
#include "Cylinder.h"
#include "Car.h"
#include "Road.h"
#include "texture.h"
#include "Wuerfel_mit_Normalen.h"

using namespace std;

const int WIDTH = 720;
const int HEIGHT = 720;
const float ASPECT = float(WIDTH) / HEIGHT;
enum COLOUR { BLACK, WHITE, RED, GREEN, BLUE, CYAN, YELLOW, MINT, MIX=99 };
int carColour = COLOUR::WHITE;
enum W_POS { LEFT, CENTER, RIGHT };
GLfloat extent = 1.0f;// Mass fuer die Ausdehnung des Modells
GLfloat x_winkel = 0.0, y_winkel = 0.0;
GLfloat position1 = 0.0f, position2 = 0.0f, carPos = 0.0f;
GLfloat wPosX = 0., wPosZ = 0., wPosZ2 = 0.;    // Position der Hindernissen
bool obstacles2 = false;
GLfloat speed = .1f;    // Game speed
int score = 0;
int score_board[3];
int yourScore = 0;
int wait_msec = 10;
enum MODE { CUSTOM_CAR_MODE=1, GAME_MODE };
int mode = MODE::CUSTOM_CAR_MODE;
enum GAME_STATUS { STOP, PLAY, PAUSE };
int status = GAME_STATUS::STOP;
GLfloat angle = 0.f;

void Motion(int x, int y)
// Maus-Bewegungen mit gedrueckter Maus-Taste
{
    GLfloat xMousePos = float(x);
    GLfloat yMousePos = float(y);
    std::cout << "Maustaste gedrueckt " << xMousePos << ", " << yMousePos << std::endl;
    // RenderScene aufrufen.
    glutPostRedisplay();
}

void MouseFunc(int button, int state, int x, int y)
// Maus-Tasten und -Bewegung abfragen
{
    GLfloat xMousePos = float(x);
    GLfloat yMousePos = float(y);

    switch (button) {
    case GLUT_LEFT_BUTTON:
        if (state == GLUT_DOWN)
            std::cout << "linke Maustaste gedrueckt " << xMousePos << ", " << yMousePos << std::endl;
        else {
            std::cout << "linke Maustaste losgelassen " << xMousePos << ", " << yMousePos << std::endl;
            if (mode == MODE::CUSTOM_CAR_MODE) {
                /* Change Color */
                if ((xMousePos >= 380 && xMousePos <= 430) &&
                    (yMousePos >= 70 && yMousePos <= 120)) {
                    carColour = COLOUR::RED;
                }
                else if ((xMousePos >= 460 && xMousePos <= 510) &&
                    (yMousePos >= 70 && yMousePos <= 120)) {
                    carColour = COLOUR::GREEN;
                }
                else if ((xMousePos >= 540 && xMousePos <= 590) &&
                    (yMousePos >= 70 && yMousePos <= 120)) {
                    carColour = COLOUR::BLUE;
                }
                else if ((xMousePos >= 620 && xMousePos <= 670) &&
                    (yMousePos >= 70 && yMousePos <= 120)) {
                    carColour = COLOUR::YELLOW;
                }
                else if ((xMousePos >= 420 && xMousePos <= 470) &&
                    (yMousePos >= 150 && yMousePos <= 200)) {
                    carColour = COLOUR::CYAN;
                }
                else if ((xMousePos >= 500 && xMousePos <= 550) &&
                    (yMousePos >= 150 && yMousePos <= 200)) {
                    carColour = COLOUR::MINT;
                }

                else if ((xMousePos >= 580 && xMousePos <= 630) &&
                    (yMousePos >= 150 && yMousePos <= 200)) {
                    carColour = COLOUR::MIX;
                }
            }
            else if (mode == MODE::GAME_MODE) {
                if (status == GAME_STATUS::STOP || status == GAME_STATUS::PAUSE) {
                    if ((xMousePos >= 290 && xMousePos <= 430) &&
                        (yMousePos >= 320 && yMousePos <= 460)) {
                        status = GAME_STATUS::PLAY;
                        cout << "PLAY!!!" << endl;
                    }
                }
                else if (status == GAME_STATUS::PLAY) {
                    status = GAME_STATUS::PAUSE;
                    cout << "PAUSE!!!" << endl;
                }
            }
        }
        break;
    }
}

void KeyboardFunc(unsigned char key, int x, int y)
{
    GLfloat xMousePos = float(x);
    GLfloat yMousePos = float(y);
    
    switch (key)
    {
    case 'a':
        if (mode == MODE::CUSTOM_CAR_MODE) {
            std::cout << "key = " << key << "  " << xMousePos << ", " << yMousePos << std::endl;
            if ((x_winkel <= 180.0f - x_winkel) || (360.0f - x_winkel <= x_winkel - 180.0f)) {
                x_winkel = 0.0f;
            }
            else {
                x_winkel = 180.0f;
            }
            y_winkel = y_winkel + 5.0f;
            if (y_winkel > 360.0)
                y_winkel = 0.0f;
        }
        else if (mode == MODE::GAME_MODE && status == GAME_STATUS::PLAY) {
            if (carPos < .6f) {   // move left
                std::cout << "key = " << key << "  " << xMousePos << ", " << yMousePos << std::endl;
                carPos += .6f;
            }
        } 
        break;
    case 'd':
        if (mode == MODE::CUSTOM_CAR_MODE) {
            std::cout << "key = " << key << "  " << xMousePos << ", " << yMousePos << std::endl;
            if ((x_winkel <= 180.0f - x_winkel) || (360.0f - x_winkel <= x_winkel - 180.0f)) {
                x_winkel = 0.0f;
            }
            else {
                x_winkel = 180.0f;
            }
            y_winkel = y_winkel - 5.0f;
            if (y_winkel < 0.0)
                y_winkel = 360.0f;
        }
        else if (mode == MODE::GAME_MODE && status == GAME_STATUS::PLAY) {
            if (carPos > -.6f) {   // move right
                std::cout << "key = " << key << "  " << xMousePos << ", " << yMousePos << std::endl;
                carPos -= .6f;
            }
        }
        break;
    case 'w':
        if (mode == MODE::CUSTOM_CAR_MODE) {
            std::cout << "key = " << key << "  " << xMousePos << ", " << yMousePos << std::endl;
            if ((y_winkel <= 180.0f - y_winkel) || (360.0f - y_winkel <= y_winkel -180.0f)) {
                y_winkel = 0.0f;
            }
            else {
                y_winkel = 180.0f;
            }
            x_winkel = x_winkel - 5.0f;
            if (x_winkel < 0.0)
                x_winkel = 360.0f;
        }
        break;
    case 's':
        if (mode == MODE::CUSTOM_CAR_MODE) {
            std::cout << "key = " << key << "  " << xMousePos << ", " << yMousePos << std::endl;
            if ((y_winkel <= 180.0f - y_winkel) || (360.0f - y_winkel <= y_winkel - 180.0f)) {
                y_winkel = 0.0f;
            }
            else {
                y_winkel = 180.0f;
            }
            x_winkel = x_winkel + 5.0f;
            if (x_winkel > 360.0)
                x_winkel = 0.0f;
        }
        break;
    default:
        break;
    }
    // RenderScene aufrufen.
    glutPostRedisplay();
}

void SpecialFunc(int key, int x, int y)
// Funktions- und Pfeil-Tasten abfragen
{
    GLfloat xMousePos = float(x);
    GLfloat yMousePos = float(y);
    if (key <= 12)
        std::cout << "GLUT_KEY_F" << key << "  " << xMousePos << ", " << yMousePos << std::endl;
    else
        switch (key) {
        case GLUT_KEY_LEFT:
            if (mode == MODE::GAME_MODE) {
                std::cout << "GLUT_KEY_LEFT " << xMousePos << ", " << yMousePos << std::endl;
                y_winkel = y_winkel + 5.0f;
                if (y_winkel > 360.0)
                    y_winkel = 0.0f;
                else {
                    if (y_winkel > 15.0f && y_winkel < 180.0) {     // limitieren das y_winkel
                        y_winkel = 15.0f;
                    }
                }
            }
            break;
        case GLUT_KEY_UP:
            if (mode == MODE::GAME_MODE) {
                std::cout << "GLUT_KEY_UP " << xMousePos << ", " << yMousePos << std::endl;
                x_winkel = x_winkel - 5.0f;
                if (x_winkel < 0.0)
                    x_winkel = 360.0f;
                else {
                    if (x_winkel < 335.0f && x_winkel > 180.0) {        // limitieren das x_winkel
                        x_winkel = 335.0f;
                    }
                }
            }
            break;
        case GLUT_KEY_RIGHT:
            if (mode == MODE::GAME_MODE) {
                std::cout << "GLUT_KEY_RIGHT " << xMousePos << ", " << yMousePos << std::endl;
                y_winkel = y_winkel - 5.0f;
                if (y_winkel < 0.0)
                    y_winkel = 360.0f;
                else {
                    if (y_winkel < 345.0f && y_winkel > 180.0) {
                        y_winkel = 345.0f;
                    }
                }
            }
            break;
        case GLUT_KEY_DOWN:
            if (mode == MODE::GAME_MODE) {
                std::cout << "GLUT_KEY_DOWN " << xMousePos << ", " << yMousePos << std::endl;
                x_winkel = x_winkel + 5.0f;
                if (x_winkel > 360.0)
                    x_winkel = 0.0f;
                else {
                    if (x_winkel > 25.0f && x_winkel < 180.0) {
                        x_winkel = 25.0f;
                    }
                }
            }
            break;
        case GLUT_KEY_PAGE_UP:
            std::cout << "GLUT_KEY_PAGE_UP " << xMousePos << ", " << yMousePos << std::endl;
            break;
        case GLUT_KEY_PAGE_DOWN:
            std::cout << "LUT_KEY_PAGE_DOWN " << xMousePos << ", " << yMousePos << std::endl;
            break;
        case GLUT_KEY_HOME:
            std::cout << "GLUT_KEY_HOME " << xMousePos << ", " << yMousePos << std::endl;
            break;
        case GLUT_KEY_END:
            std::cout << "GLUT_KEY_END " << xMousePos << ", " << yMousePos << std::endl;
            glLoadIdentity();
            break;
        case GLUT_KEY_INSERT:
            std::cout << "GLUT_KEY_INSERT " << xMousePos << ", " << yMousePos << std::endl;
            break;
        }
    // RenderScene aufrufen.
    glutPostRedisplay();
}

void setCam(int mode) {     // set Camera Position
    switch (mode)
    {
    case MODE::GAME_MODE:
        gluLookAt(3., 1.4, 0., 0., 0., 0., 0., 1., 0.);
        break;
    default:
        gluLookAt(0., 1., 3., 0., 0., 0., 0., 1., 0.);
        break;
    }
}

void setwPosZ() {       // set z Position Hindernisse
    srand(time(NULL));

    int randPos = rand() % 3;
    int randPos2;

    switch (randPos) {
    case W_POS::LEFT:
        wPosZ = .6f;
        randPos2 = (rand() % 2) + 1;  // Random Position CENTER oder RIGHT
        break;
    case W_POS::CENTER:
        wPosZ = 0.f;
        randPos2 = (rand() % 2) * 2;    // LEFT oder RIGHT
        break;
    case W_POS::RIGHT:
        wPosZ = -.6f;
        randPos2 = (rand() % 2);    // LEFT oder CENTER
        break;
    }

    switch (randPos2)
    {
    case W_POS::LEFT:
        wPosZ2 = .6f;
        break;
    case W_POS::CENTER:
        wPosZ2 = .0f;
        break;
    case W_POS::RIGHT:
        wPosZ2 = -.6f;
        break;
    default:
        break;
    }
}

void mainMenu(int item)
{
    switch (item)
    {
    case 1:
        std::cout << "Exit" << std::endl;
        exit(0);
    }
}

void backGroundColor(int item)      // Change Background color
{
    switch (item)
    {
    case 1:
    {
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);	// Hintergrundfarbe weiss definieren 
        // RenderScene aufrufen.
        glutPostRedisplay();
        break;
    }
    case 2:
    {
        glClearColor(1.0f, 0.0f, 0.0f, 1.0f);	// Hintergrundfarbe rot definieren 
        // RenderScene aufrufen.
        glutPostRedisplay();
        break;
    }
    case 3:
    {
        glClearColor(0.33f, 0.225f, 0.0f, 1.0f);// Hintergrundfarbe gold definieren 
        // RenderScene aufrufen.
        glutPostRedisplay();
        break;
    }
    case 4:
    {
        glClearColor(0.8f, 0.8f, 0.8f, 1.0f);// Hintergrundfarbe gray definieren 
        // RenderScene aufrufen.
        glutPostRedisplay();
        break;
    }
    }
}

void resetCustomMode() {
    x_winkel = 0.0;
    y_winkel = 0.0;
    if (status == GAME_STATUS::STOP) {
        status = GAME_STATUS::STOP;
    }
    else {
        status = GAME_STATUS::PAUSE;
    }
    yourScore = 0;
}

void resetGameMode() {
    if (status == GAME_STATUS::STOP || status == GAME_STATUS::PLAY) {
        status = GAME_STATUS::STOP;
        x_winkel = 0.0;
        y_winkel = 0.0;
        score = 0;
        wPosX = 0.;
        carPos = 0.;
        speed = .1f;
        obstacles2 = false;
        setwPosZ();
    }
    else {
        status = GAME_STATUS::PAUSE;
    }
}

void changeMode(int item)
{
    switch (item)
    {
    case 1:
    {
        mode = MODE::CUSTOM_CAR_MODE;
        // RenderScene aufrufen.
        resetCustomMode();
        glutPostRedisplay();
        break;
    }
    case 2:
    {
        mode = MODE::GAME_MODE;
        // RenderScene aufrufen.
        resetGameMode();
        glutPostRedisplay();
        break;
    }
    }
}

void menuInit()     // Menü Initialiserung
{
    // Unter-Menu
    int submenu1, submenu2;
    submenu1 = glutCreateMenu(backGroundColor);
    glutAddMenuEntry("BackgroundColor WHITE", 1);
    glutAddMenuEntry("BackgroundColor RED", 2);
    glutAddMenuEntry("BackgroundColor GOLD", 3);
    glutAddMenuEntry("BackgroundColor GRAY", 4);

    submenu2 = glutCreateMenu(changeMode);
    glutAddMenuEntry("Custom Car Mode", 1);
    glutAddMenuEntry("Game Mode", 2);

    // Haupt-Menu
    glutCreateMenu(mainMenu);
    glutAddSubMenu("Background color", submenu1);
    glutAddSubMenu("Change Mode", submenu2);
    glutAddMenuEntry("Exit", 1);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void lightInit() {      // Licht Initialiserung
    GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat mat_shininess[] = { 50.0 };

    GLfloat light_ambient[] = { 0.0, 0.0, 0.0, 1.0 };
    GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat light_position[] = { 0.0, 1.0, 2.0, 0.0 };
    glShadeModel(GL_SMOOTH);

    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
    // Normalen fuer korrekte Beleuchtungs-Berechnung normalisieren
    glEnable(GL_NORMALIZE);
}

void Init()	
{
   // Hier finden jene Aktionen statt, die zum Programmstart einmalig 
   // durchgeführt werden müssen
   glClearColor(0.8f, 0.8f, 0.8f, 1.0f);
   menuInit();
   lightInit();
   glEnable(GL_DEPTH_TEST);
   glutMotionFunc(Motion);
   glutMouseFunc(MouseFunc);
   glutSpecialFunc(SpecialFunc);
   glutKeyboardFunc(KeyboardFunc);
}

void drawText(string text, int x, int y, GLfloat size = 1.f, GLfloat width = 1.f) {
    const char* data = text.data();

    glDisable(GL_LIGHTING);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0., WIDTH, HEIGHT, 0.);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glTranslatef(x, y, 0.1f);
    glScalef(size, size, 1.);

    /*glRasterPos2i(x, y);      // draw Text with glut Bitmap
    glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24, (unsigned char*)data);*/
    glPushMatrix();     // draw Text with glut Stroke
    glScalef(.15, .15, 1.);
    glRotatef(180.f, 1, 0, 0);
    glLineWidth(width);
    glutStrokeString(GLUT_STROKE_MONO_ROMAN, (unsigned char*)data);
    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glEnable(GL_LIGHTING);
}

void carColourTable() {
    glDisable(GL_LIGHTING);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0., WIDTH, HEIGHT, 0.);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glBegin(GL_QUADS);
    glColor4f(1., 0., 0., 1.);      // Red
    glVertex2f(380, 120);
    glVertex2f(430, 120);
    glVertex2f(430, 70);
    glVertex2f(380, 70);
    glEnd();
    glBegin(GL_QUADS);
    glColor4f(0., 1., 0., 1.);      // Green
    glVertex2f(460, 120);
    glVertex2f(510, 120);
    glVertex2f(510, 70);
    glVertex2f(460, 70);
    glEnd();
    glBegin(GL_QUADS);
    glColor4f(0., 0., 1., 1.);      // Blue
    glVertex2f(540, 120);
    glVertex2f(590, 120);
    glVertex2f(590, 70);
    glVertex2f(540, 70);
    glEnd();
    glBegin(GL_QUADS);
    glColor4f(1.0f, 1.0f, 0.0f, 1.0f);      // Yellow
    glVertex2f(620, 120);
    glVertex2f(670, 120);
    glVertex2f(670, 70);
    glVertex2f(620, 70);
    glEnd();
    glBegin(GL_QUADS);
    glColor4f(0.0f, 1.0f, 1.0f, 1.0f);      // Cyan
    glVertex2f(420, 200);
    glVertex2f(470, 200);
    glVertex2f(470, 150);
    glVertex2f(420, 150);
    glEnd();
    glBegin(GL_QUADS);
    glColor4f(0.741f, 1.f, 0.741f, 1.0f);      // MINT
    glVertex2f(500, 200);
    glVertex2f(550, 200);
    glVertex2f(550, 150);
    glVertex2f(500, 150);
    glEnd();
    glBegin(GL_QUADS);                  // MIX
    glColor4f(1.0f, 0.0f, 1.0f, 1.0f); //MAGENTA
    glVertex2f(580, 200);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
    glVertex2f(630, 200);
    glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
    glVertex2f(630, 150);
    glColor4f(0.0f, 0.0f, 1.0f, 1.0f); //BLAU
    glVertex2f(580, 150);
    glEnd();

    glBegin(GL_QUADS);
    glColor4f(1., 1., 1., .6);
    glVertex2f(350, 230);
    glVertex2f(700, 230);
    glVertex2f(700, 20);
    glVertex2f(350, 20);
    glEnd();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glEnable(GL_LIGHTING);
}

void playButton() {
    glDisable(GL_LIGHTING);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0., WIDTH, HEIGHT, 0.);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glBegin(GL_TRIANGLES);
    glColor4f(1., 1., 1., 1.);
    glVertex2f(300, 450);
    glVertex2f(420, 390);
    glVertex2f(300, 330);
    glEnd();
    glBegin(GL_QUADS);
    glColor4f(.6, .6, .6, 1.);
    glVertex2f(430, 460);
    glVertex2f(290, 460);
    glVertex2f(290, 320);
    glVertex2f(430, 320);
    glEnd();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glEnable(GL_LIGHTING);
}

void scoreBoard() {
    glColor4f(0., 0., 0., 1.);
    drawText("Score Board", 270, 180);
    string score_board_text = "1. " + to_string(score_board[0]) + "\n2. " + to_string(score_board[1]) + "\n3. " + to_string(score_board[2]);
    drawText(score_board_text, 270, 210, .8f);

    glDisable(GL_LIGHTING);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0., WIDTH, HEIGHT, 0.);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glColor4f(1, 0.415, 0, 1.);
    glBegin(GL_QUADS);
    glVertex2f(200, 500);
    glVertex2f(520, 500);
    glVertex2f(520, 150);
    glVertex2f(200, 150);
    glEnd();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glEnable(GL_LIGHTING);
}

void RenderScene() //Zeichenfunktion
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Hier befindet sich der Code der in jedem Frame ausgefuehrt werden muss
    glLoadIdentity ();   // Aktuelle Model-/View-Transformations-Matrix zuruecksetzen
    setCam(mode);   // Camera einstellen

    glPushMatrix();
        glScalef(.8, .8, .8);
        if (mode == MODE::GAME_MODE) {
            glRotatef(x_winkel, 1.0f, 0.0f, 0.0f);
            glRotatef(y_winkel, 0.0f, 1.0f, 0.0f);
        }
        glPushMatrix();
            if (mode == MODE::CUSTOM_CAR_MODE) {    // in Custom Car Mode
                glRotatef(x_winkel, 1.0f, 0.0f, 0.0f);
                glRotatef(y_winkel, 0.0f, 1.0f, 0.0f);
            }
            else if (mode == MODE::GAME_MODE) {   // in Game Mode
                glTranslatef(1., 0., 0.);
                glTranslatef(0., 0., carPos);
            }
            Car(carColour, angle);
        glPopMatrix();

        if (mode == MODE::CUSTOM_CAR_MODE) {
            glPushMatrix();     // Hintergrund texture
            textureInit("brick_wall.jpg");
            glEnable(GL_TEXTURE_2D);
            glBegin(GL_QUADS);
            glNormal3f(0.0f, 0.0f, 1.0f);	// light Normal vec
            glColor4f(1., 1., 1., 1.);
            glTexCoord2f(0.0f, 0.0f);   glVertex3f(-2.5, -.25, -2.);
            glTexCoord2f(1.0f, 0.0f);   glVertex3f(2.5, -.25, -2.);
            glTexCoord2f(1.0f, 1.0f);   glVertex3f(2.5, 3., -2.);
            glTexCoord2f(0.0f, 1.0f);   glVertex3f(-2.5, 3., -2.);
            glEnd();
            glDisable(GL_TEXTURE_2D);

            glBegin(GL_QUADS);
            glNormal3f(0.0f, 1.0f, 0.0f); // light Normal vec
            glColor4f(.6, .6, .6, 1.);
            glVertex3f(-2.5, -.25, 1.5);
            glVertex3f(2.5, -.25, 1.5);
            glVertex3f(2.5, -.25, -2.);
            glVertex3f(-2.5, -.25, -2.);
            glEnd();
            glPopMatrix();

            glPushMatrix();
            // Hint
            glColor4f(0.0f, 1.0f, 1.0f, 1.0f); //CYAN
            string hint = "W, A, S, D:\nCar Ratation\n\nRight Mouse Click:\nMenu";
            drawText(hint, 20, 30, 1., 2.);
            glColor4f(0., 0., 0., 1.);
            drawText("CAR COLOUR", 450, 50);
            carColourTable();
            glPopMatrix();
        }
        else if (mode == MODE::GAME_MODE) {   // in Game Mode
            // Hint
            glColor4f(0., 0., 0., 1.);
            string hint = "A, D:\nCar Move\n\nLeft Mouse Click:\nPause";
            drawText(hint, 20, 60, 1., 1.);

            switch (status) {
            case GAME_STATUS::STOP:
                if (yourScore) {
                    glPushMatrix();
                    string score_text = to_string(yourScore); // Text
                    string text = "Your Score: " + score_text;
                    glColor4f(0.0f, 1.0f, 0.0f, 1.0f); //GRUEN
                    drawText(text, 240, 290, 1.1f, 1.5f);
                    glPopMatrix();
                }
                glPushMatrix();
                    playButton();
                    scoreBoard();
                glPopMatrix();
                break;
            case GAME_STATUS::PAUSE:
                glPushMatrix();
                    playButton();
                    scoreBoard();
                glPopMatrix();
                break;
            default:
                break;
            }
            glPushMatrix();
                Road(position1, position2, y_winkel);     // Road
                string score_text = to_string(score); // Score
                string text = "Score: " + score_text;
                glColor4f(1., 0., 0., 1.);
                drawText(text, 20, 30);

                glPushMatrix();     // Hindernisse
                    glTranslatef(wPosX, 0., 0.);
                    glPushMatrix();
                    glTranslatef(-9., 0., wPosZ);
                    Wuerfel_mit_Normalen(.3);
                    glPopMatrix();
                    if (obstacles2) { // 2. Hindernis anzeigen
                        glPushMatrix();
                        glTranslatef(-9., 0., wPosZ2);
                        Wuerfel_mit_Normalen(.3);
                        glPopMatrix();
                    }
                glPopMatrix();
            glPopMatrix();
        }
    glPopMatrix();

    glutSwapBuffers();
    glFlush();
}

void Reshape(int width,int height)
{
    // Hier finden die Reaktionen auf eine Veränderung der Größe des 
    // Graphikfensters statt
    // Matrix fuer Transformation: Frustum->viewport
    glMatrixMode(GL_PROJECTION);
    // Aktuelle Transformations-Matrix zuruecksetzen
    glLoadIdentity();
    // Viewport definieren
    glViewport(0, 0, width, height);
    // Frustum definieren (siehe unten)
    /*glOrtho( GLdouble left, GLdouble right,
    GLdouble bottom, GLdouble top,
    GLdouble near, GLdouble far );*/
    //glOrtho(-1., 1., -1., 1., 0.0, 10.0);
    gluPerspective(45., 1., 0.1, 10.0);
    // Matrix fuer Modellierung/Viewing
    glMatrixMode(GL_MODELVIEW);

}

void Animate (int value)    
{
   // Hier werden Berechnungen durchgeführt, die zu einer Animation der Szene  
   // erforderlich sind. Dieser Prozess läuft im Hintergrund und wird alle 
   // 1000 msec aufgerufen. Der Parameter "value" wird einfach nur um eins 
   // inkrementiert und dem Callback wieder uebergeben. 
   //std::cout << "value=" << value << std::endl;
   // RenderScene aufrufen
   glutPostRedisplay();
   // Timer wieder registrieren - Animate wird so nach 10 msec mit value+=1 aufgerufen.
   angle += 8.f;
   if (status == GAME_STATUS::PLAY) { // wenn Game spielt wird
       if (position1 < 36.0f) { // Road Animation
           position1 += speed;
       }
       else {
           position1 = 0.0f;
       }
       if (position2 < 18.0f) {
           position2 += speed;
       }
       else {
           position2 = -18.0f;
       }
       if (wPosX < 11.) {
           if (int(wPosX) == 9. && (carPos == wPosZ || (obstacles2 && carPos == wPosZ2))) { // wenn Auto gegen Hindernis stößt
               cout << "STOP!!!" << endl;
               yourScore = score;
               if (score >= score_board[0]) {
                   score_board[2] = score_board[1];
                   score_board[1] = score_board[0];
                   score_board[0] = score;
               }
               else if (score >= score_board[1]) {
                   score_board[2] = score_board[1];
                   score_board[1] = score;
               }
               else if (score >= score_board[2]) {
                   score_board[2] = score;
               }
               status = GAME_STATUS::STOP;
               resetGameMode();
           }
           else {
               wPosX += speed;  // Animation Hindernisse
               score += 1;
               if (score == 300) {  // speed up
                   speed += .05f;
               }
               else if (score == 600) {
                   speed += .05f;
               }
           }
       }
       else {
           wPosX = 0.0f;
           setwPosZ();
           obstacles2 = rand() % 2; // random ob es gibt 2. Hindernis oder nicht
           if (score >= 450) {
               obstacles2 = true;
           }
       }
   }

   glutTimerFunc(wait_msec, Animate, ++value);
}

int main(int argc, char **argv)
{
   glutInit( &argc, argv );                // GLUT initialisieren
   glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
   glutInitWindowSize( WIDTH, HEIGHT );         // Fenster-Konfiguration
   glutInitWindowPosition(320, 20);
   glutCreateWindow( "Car-3D_Minh Thanh Pham-762438_Hai Ninh Bui-757973" );   // Fenster-Erzeugung
   glutDisplayFunc( RenderScene );         // Zeichenfunktion bekannt machen
   glutReshapeFunc( Reshape );
   // TimerCallback registrieren; wird nach 10 msec aufgerufen mit Parameter 0  
   glutTimerFunc(wait_msec, Animate, 0);
   Init();
   glutMainLoop();
   return 0;
}