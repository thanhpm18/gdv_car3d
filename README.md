# Graphische Datenverarbeitung, WS2021 Alternative Prüfung: „Rendering Competition“
Authors:
- Ninh Bui, 757973
- Minh Thanh Pham, 762438

## Aufgabenstellung

Mit den ersten drei Pflichtaufgaben haben Sie einen Einblick in die Grundlagen der graphischen Programmierung erhalten. Um die gewonnenen Kenntnisse zu vertiefen, starten wir heute die Rendering Competition! Sie haben nun sechs Wochen Zeit, Ihrer Kreativität freien Lauf zu lassen und mittels der gelernten Techniken ein komplexeres graphisches Programm zu entwerfen. Das Thema der Rendering Competition lautet: Fahrzeuge - ansonsten sind Sie inhaltlich komplett frei in der Gestaltung.
Anhand dieser Aufgabe wird Ihre GDV Note ermittelt, Zusatzpunkte gibt es in diesem Semester keine.

## Bewertungskriterien

Auch wenn es keine direkten inhaltlichen Vorgaben gibt, so sind die Noten an sechs Kriterien gebunden, die Ihr Programm aufweisen muss. Pro erfülltes Kriterium erhalten Sie Punkte. Aus diesen Punkten wird dann die Note abgeleitet.
Grob nach aufsteigendem Schwierigkeitsgrad sortiert erhalten Sie Punkte für:
• Kreativität!
• einen komplexen, mehrfach verzweigten Szenegraph mit mind. 3 Ebenen
• sinnvolle, abhängige Animationen
• Userinput
• Licht / Beleuchtung
• Texturen

## Project: Racing Car 3D Game

### Setup
1. Run `aufgabe.cpp`or executable file.
2. Right click on screen to open menu.
3. Option `Background color`: to change background of the game.
4. Option `Change mode`: to change the game modes:
- Mode `Custom car mode`: to change your car appearance. Using w,a,s,d to rotate your car on display.
- Mode `Game mode`: to play or reset the game. 
5. Option `Exit`: to exit the game.

### Game play instruction

- Using a, d buttons to avoid all the blocks on the road. The speed of your car will increase time after time. 
- Using left, right, up, down buttons to change your view point on display.
- Right clicking and choosing Mode "Game mode" to restart the game. 
- Your game score will be showed on the right-top corner of the display.
- After every game you will see the highest score board.

## Scene Graph
https://gitlab.com/thanhpm18/gdv_car3d/-/blob/master/Szenegraph.pdf

## Trial Video
![Racing Car 3D Game](https://gitlab.com/thanhpm18/gdv_car3d/-/blob/master/GDV_Zusatzleistung_Video.mp4)
https://gitlab.com/thanhpm18/gdv_car3d/-/blob/master/GDV_Zusatzleistung_Video.mp4

## Git for sourcecode and others
https://gitlab.com/thanhpm18/gdv_car3d
