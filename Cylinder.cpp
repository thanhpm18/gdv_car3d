#define _USE_MATH_DEFINES
#include <math.h>
#include <GL/freeglut.h>
#include "Cylinder.h"



void Circle(GLfloat r, GLfloat x, GLfloat y, GLfloat z) {
    float i = 0.0f;

    glBegin(GL_POLYGON);

    glVertex3f(x, y, z); // Center
    for (i = 0.0f; i <= 360; i++)
        glVertex3f(r * cos(M_PI * i / 180.0) + x, r * sin(M_PI * i / 180.0) + y, z);

    glEnd();
}

void DrawStarFilled(float x, float y, float z, float radius, int numPoints)
{
    const float DegToRad = M_PI / 180;

    glBegin(GL_TRIANGLE_FAN);
    int count = 1;
    glVertex3f(x, y, z);
    for (int i = 0; i <= numPoints * 2; i++) {
        float DegInRad = i * 360.0 / (numPoints * 2) * DegToRad;
        if (count % 2 != 0)
            glVertex3d(x + cos(DegInRad) * radius, y + sin(DegInRad) * radius, z);
        else
            glVertex3d((x + cos(DegInRad) * radius / 2), (y + sin(DegInRad) * radius / 2), z);
        count++;
    }

    glEnd();
}

void Cylinder(GLfloat radius, GLfloat height)
{
    glPushMatrix();
    glTranslated(0., 0., -height / 2);
    glColor4f(0.0f, 0.0f, 0.0f, 1.0f); //SCHWARZ
    GLUquadricObj* quadratic_obj;
    quadratic_obj = gluNewQuadric();
    gluCylinder(quadratic_obj, radius, radius, height, 32, 32);
    glNormal3f(0.0f, 0.0f, 1.0f);	// light Normal vec
    Circle(radius, 0., 0., height);
    glNormal3f(0.0f, 0.0f, -1.0f);	// light Normal vec
    Circle(radius, 0., 0., 0.);
    glPopMatrix();
}

void CylinderS(GLfloat radius, GLfloat height)
{
    glPushMatrix();
    glTranslated(0., 0., -height / 2);
    glColor4f(0.0f, 0.0f, 0.0f, 1.0f); //SCHWARZ
	GLUquadricObj* quadratic_obj;
	quadratic_obj = gluNewQuadric();
	gluCylinder(quadratic_obj, radius, radius, height, 32, 32);
    glNormal3f(0.0f, 0.0f, 1.0f);	// light Normal vec
    Circle(radius, 0., 0., height);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
    DrawStarFilled(0., 0., height + .0001, radius*0.8, 8);
    glNormal3f(0.0f, 0.0f, -1.0f);	// light Normal vec
    glColor4f(0.0f, 0.0f, 0.0f, 1.0f); //SCHWARZ
    Circle(radius, 0., 0., 0.);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f); //WEISS
    DrawStarFilled(0., 0., 0. - .0001, radius * 0.8, 8);
    glPopMatrix();
}

void CylinderH(GLfloat radius, GLfloat height)
{
    glPushMatrix();
    glTranslated(0., 0., -height / 2);
    glColor4f(0.0f, 0.0f, 0.0f, 1.0f); //SCHWARZ
    GLUquadricObj* quadratic_obj;
    quadratic_obj = gluNewQuadric();
    gluCylinder(quadratic_obj, radius, radius, height, 64, 64);
    glPopMatrix();
}
